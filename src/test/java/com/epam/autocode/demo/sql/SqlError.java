package com.epam.autocode.demo.sql;

public class SqlError {

    private String sqlState;

    private int errorCode;

    private String message;

    public SqlError(String sqlState) {
        this.sqlState = sqlState;
    }

    public SqlError(String sqlState, int errorCode, String message) {
        this.sqlState = sqlState;
        this.errorCode = errorCode;
        this.message = message;
    }

    public String getSqlState() {
        return sqlState;
    }

    public void setSqlState(String sqlState) {
        this.sqlState = sqlState;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "SqlError{" +
                "sqlState='" + sqlState + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
