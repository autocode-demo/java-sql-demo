package com.epam.autocode.demo.sql;

import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

class SqlQueriesTest {

    @Test
    void shouldReturnTwoStudents_whenSelectingAllStudents() {
        try (var connection = H2DatabaseConnector.getConnection();
             var statement = connection.createStatement()) {

            // create table
            statement.execute("CREATE TABLE STUDENTS(id INTEGER not null, name VARCHAR(255) not null, PRIMARY KEY ( id ))");

            // insert two students
            statement.execute("INSERT INTO STUDENTS(id, name) VALUES (1, 'John'), (2, 'Mike')");

            // get the implemented query
            var studentsQuery = SqlQueries.findStudents();

            //simple assertions for sanity check
            assertThat(studentsQuery)
                    .withFailMessage("Query should not be blank")
                    .isNotBlank()
                    .withFailMessage("Query should contain SELECT statement")
                    .containsIgnoringCase("SELECT");

            // run sql query and verify result
            var resultSet = statement.executeQuery(studentsQuery);
            int studentCount = 0;
            while (resultSet.next()) {
                studentCount++;
            }
            assertThat(studentCount).isEqualTo(2);

        } catch (SQLException e) {
            fail(String.format("Database query error: %s", H2DatabaseConnector.getSqlError(e)));
        }
    }
}